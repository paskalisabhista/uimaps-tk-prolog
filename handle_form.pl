:- use_module(library(http/http_server)).
:- use_module(library(http/http_dispatch)).
:- use_module(library(http/http_client)).
:- use_module(library(http/http_json)).

:- http_handler('/submit', handle_data, [method(post)]).

:- consult('uimaps.pl').

server_handle(Port) :-
        http_server(http_dispatch, [port(Port)]).

handle_data(Request) :-
    http_read_data(Request, Data, []),
    memberchk(awal=Awal, Data),
    memberchk(tujuan=Tujuan, Data),
    (   valid_input(Awal, Tujuan) ->
        rute_jarak(Awal, Tujuan, Rute, Jarak),
        lokasi_terdekat(Awal, Terdekat, JarakLokasi),
        halte_terdekat(Awal, HalteTerdekat),
        rute_bikun(Awal, Tujuan, MenungguDi, BerhentiDi),
        JSON = json(["Rute"=Rute, "JarakRute"=Jarak, "LokasiTerdekat"=Terdekat, "JarakLokasi"=JarakLokasi, "HalteTerdekat"= HalteTerdekat, "MenungguDi"=MenungguDi, "BerhentiDi"=BerhentiDi]),reply_json(JSON);
    format("Data yang dimasukkan salah")).

valid_input(X, Y):-
     adadiui(X),
     adadiui(Y).

:- initialization(server_handle(5000)).
