:- use_module(library(http/html_write)).
:- use_module(library(http/thread_httpd)).
:- use_module(library(http/http_dispatch)).
:- use_module(library(http/http_client)).
:- use_module(library(http/http_json)).

:- http_handler('/form', my_handler, []).
:- http_handler('/submit', handle_data, [method(post)]).

:- consult('uimaps.pl').

server(Port) :-
        http_server(http_dispatch, [port(Port)]).

my_handler(_Request) :-
    reply_html_page(title('My Form'),[form([action='/submit', method=post],
       [label([for=awal], 'Lokasi Awal :'),
        input([type=text, name=awal]),
        br([]),
        label([for=tujuan], 'Tujuan :'),
        input([type=text, name=tujuan]),
        br([]),
        input([type=submit, value='Submit'])
    ])]).

valid_input(X, Y):-
     adadiui(X),
     adadiui(Y).

handle_data(Request) :-
    http_read_data(Request, Data, []),
    memberchk(awal=Awal, Data),
    memberchk(tujuan=Tujuan, Data),
    (   valid_input(Awal, Tujuan) ->
        rute_jarak(Awal, Tujuan, Rute, Jarak),
        lokasi_terdekat(Awal, Terdekat, JarakLokasi),
        halte_terdekat(Awal, HalteTerdekat),
        rute_bikun(Awal, Tujuan, MenungguDi, BerhentiDi),
        JSON = json(["Rute"=Rute, "JarakRute"=Jarak, "LokasiTerdekat"=Terdekat, "JarakLokasi"=JarakLokasi, "HalteTerdekat"= HalteTerdekat, "MenungguDi"=MenungguDi, "BerhentiDi"=BerhentiDi]),reply_json(JSON);
    format("Data yang dimasukkan salah")).

:- initialization(server(5000)).
