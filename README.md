<a name="readme-top"></a>

<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://gitlab.com/paskalisabhista/uimaps-tk-prolog">
    <img src="icons8-map-64.png" alt="Logo" width="120" height="120">
  </a>

<h3 align="center">UIMaps</h3>

  <p align="center">
    A map project made with Prolog.
    <br />
    <a href="https://gitlab.com/paskalisabhista/uimaps-tk-prolog"><strong>Explore the docs »</strong></a>
    <br />
    <br />
  </p>
</div>

<!-- ABOUT THE PROJECT -->
## Author

- [Muhammad Rifqi Praditya](https://gitlab.com/NitraditMS)
- [Nabilah Adani Nurulizah](https://gitlab.com/nabilah.adani)
- [Nur Nisrina](https://gitlab.com/Nisrinous)
- [Paskalis Abhista Bagaskara Yustiyanto](https://gitlab.com/paskalisabhista)

### Built With

#### Backend

* [Prolog](https://www.happstack.com/)

#### Frontend

* [http/html_write](https://www.swi-prolog.org/pldoc/man?section=htmlwrite)

<!-- GETTING STARTED -->
## Getting Started

This is how you may setting up the project locally:

### Prerequisites

* [SWI-Prolog](https://www.swi-prolog.org/download/stable)

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/paskalisabhista/uimaps-tk-prolog.git
   ```
2. Open prolog on terminal
   ```sh
   swipl
   ```
3. In folder uimaps-tk-prolog, run the program locally
   ```sh
   consult('handle_form.pl').
   ```

<!-- FEATURES -->
## Features

- [x] `lokasi_terdekat`

`lokasi_terdekat` will return the LokasiTerdekat which represents the closest location from the starting point and also the JarakLokasi representing the distance between the starting point and the closest location.

- [x] `rute` & `jarak terdekat`

This predicate will find the closest route between two locations and its distance.

- [x] `rute` & `halte_terdekat`

This predicate will find a route using bikun between two locations and a location's nearest haltebikun.

## Graph

<div align="center">
  <a href="https://gitlab.com/paskalisabhista/uimaps-tk-prolog">
    <img src="graf-uimaps.png" alt="graf">
  </a>

</div>

##